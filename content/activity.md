Title: 101camp 12py Activity Status
Date: 2020-10-23 22:33
Category: python
Tags: pelican, gitlab
Authors: one-eye-Q
Summary: 101camp12py Activity Status
```bash
******************************
Top 5 Commit push
author.username
junstudy      53
oneeyeQ       31
jingxuanz     29
ZoomQuiet     10
wanghua101     1
dtype: int64
******************************
Top 5 Commit-comments
author.username
ZoomQuiet        24
junstudy          6
oneeyeQ           4
izhangshiying     2
jingxuanz         1
dtype: int64
******************************
Top 5 issue comment
author.username
oneeyeQ       57
junstudy      34
ZoomQuiet     33
jingxuanz     28
wanghua101     2
dtype: int64
```