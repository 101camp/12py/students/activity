# 101camp 12py Activity Status

This project is for displaying [101camp 12py student activity](https://101camp.gitlab.io/12py/students/activity/) status through GitLab Page.

- top5 ci ~ 每个分支所有 commit
- top5 cc ~ 每个 commit 可能的 comments
- top5 is ~ 每个 Issue
- top5 ic ~ 每个 Issue 可能的 comments

## logging

- 201020 2149 one-eye-Q add url
- 201020 2105 one-eye-Q init.
